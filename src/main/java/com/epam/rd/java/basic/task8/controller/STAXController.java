package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	List<Flower> flowers = new LinkedList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void todo() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlFileName);


		for (int i=0; i<document.getDocumentElement().getChildNodes().getLength(); i++) {
			Node item = document.getDocumentElement().getChildNodes().item(i);
			if (item.getNodeName().equals("flower")) {
				Flower newFlower = new Flower();

				NodeList children = item.getChildNodes();
				for (int j=0; j<children.getLength(); j++) {
					Node innerItem = children.item(j);
					switch (innerItem.getNodeName()) {
						case "name" : {
							newFlower.setName(innerItem.getTextContent());
							break;
						}
						case "soil" : {
							newFlower.setSoil(innerItem.getTextContent());
							break;
						}
						case "origin" : {
							newFlower.setOrigin(innerItem.getTextContent());
							break;
						}
						case "multiplying" : {
							newFlower.setMultiplying(innerItem.getTextContent());
							break;
						}
						case "visualParameters" : {
							selectVisualParameters(newFlower, innerItem);
							break;
						}
						case "growingTips" : {
							selectGrowingTips(newFlower, innerItem);
							break;
						}
					}
				}
				flowers.add(newFlower);
			}
		}

	}

	private void selectVisualParameters(Flower flower, Node node) {
		NodeList childNodes = node.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++) {
			Node item = childNodes.item(i);

			switch (item.getNodeName()) {
				case "stemColour" : {
					flower.getVisualParameters().put("stemColour", item.getTextContent());
					break;
				}
				case "leafColour" : {
					flower.getVisualParameters().put("leafColour", item.getTextContent());
					break;
				}
				case "aveLenFlower" : {
					flower.getVisualParameters().put("aveLenFlower", item.getTextContent() + String.format("[%s]", item.getAttributes().getNamedItem("measure")));
					break;
				}
			}
		}
	}

	private void selectGrowingTips(Flower flower, Node node) {
		NodeList childNodes = node.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++) {
			Node item = childNodes.item(i);

			switch (item.getNodeName()) {
				case "tempreture" : {
					flower.getGrowingTips().put("tempreture", item.getTextContent() + String.format("[%s]", item.getAttributes().getNamedItem("measure")));
					break;
				}
				case "lighting" : {
					flower.getGrowingTips().put("lighting", String.format("[%s]", item.getAttributes().getNamedItem("lightRequiring")));
					break;
				}
				case "watering" : {
					flower.getGrowingTips().put("watering", item.getTextContent() + String.format("[%s]", item.getAttributes().getNamedItem("measure")));
					break;
				}
			}
		}
	}


	public void createDocument() throws ParserConfigurationException, TransformerException {
		// creating DocBuilderFactory
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		// initialize document builder
		DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
		// creating document object for future XML
		Document document = documentBuilder.newDocument();
		// creating root tag for XML
		Element root = document.createElement("flowers");
		//исправление
		root.setAttribute("xmlns", "http://www.nure.ua");
		root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

		for (Flower item : flowers) {
			Element flowerEl = document.createElement("flower");

			Element nameEl = document.createElement("name");
			nameEl.setTextContent(item.getName());
			flowerEl.appendChild(nameEl);

			Element soilEl = document.createElement("soil");
			soilEl.setTextContent(item.getSoil());
			flowerEl.appendChild(soilEl);

			Element originEl = document.createElement("origin");
			originEl.setTextContent(item.getOrigin());
			flowerEl.appendChild(originEl);

			//-------------

			Element visualParametersEl = document.createElement("visualParameters");

			Element stemColourEl = document.createElement("stemColour");
			stemColourEl.setTextContent((String) item.getVisualParameters().get("stemColour"));
			visualParametersEl.appendChild(stemColourEl);

			Element leafColourEl = document.createElement("leafColour");
			leafColourEl.setTextContent((String) item.getVisualParameters().get("leafColour"));
			visualParametersEl.appendChild(leafColourEl);

			Element aveLenFlowerEl = document.createElement("aveLenFlower");
			String temp = ((String) item.getVisualParameters().get("aveLenFlower"));
			aveLenFlowerEl.setTextContent(temp.substring(0, temp.indexOf("[")));
			aveLenFlowerEl.setAttribute(temp.substring(temp.indexOf("[")+1, temp.indexOf("=")), temp.substring(temp.indexOf("\"")+1, temp.lastIndexOf("\"")));
			visualParametersEl.appendChild(aveLenFlowerEl);

			flowerEl.appendChild(visualParametersEl);

			//-------------

			Element growingTipsEl = document.createElement("growingTips");

			Element tempretureEl = document.createElement("tempreture");
			temp = ((String) item.getGrowingTips().get("tempreture"));
			tempretureEl.setTextContent(temp.substring(0, temp.indexOf("[")));
			tempretureEl.setAttribute(temp.substring(temp.indexOf("[")+1, temp.indexOf("=")), temp.substring(temp.indexOf("\"")+1, temp.lastIndexOf("\"")));
			growingTipsEl.appendChild(tempretureEl);

			Element lightingEl = document.createElement("lighting");
			temp = ((String) item.getGrowingTips().get("lighting"));
			lightingEl.setAttribute(temp.substring(temp.indexOf("[")+1, temp.indexOf("=")), temp.substring(temp.indexOf("\"")+1, temp.lastIndexOf("\"")));
			growingTipsEl.appendChild(lightingEl);

			Element wateringEl = document.createElement("watering");
			temp = ((String) item.getGrowingTips().get("watering"));
			wateringEl.setTextContent(temp.substring(0, temp.indexOf("[")));
			wateringEl.setAttribute(temp.substring(temp.indexOf("[")+1, temp.indexOf("=")), temp.substring(temp.indexOf("\"")+1, temp.lastIndexOf("\"")));
			growingTipsEl.appendChild(wateringEl);

			flowerEl.appendChild(growingTipsEl);

			//-------------

			Element multiplyingEl = document.createElement("multiplying");
			multiplyingEl.setTextContent(item.getMultiplying());
			flowerEl.appendChild(multiplyingEl);

			root.appendChild(flowerEl);
		}

		StreamResult result = new StreamResult(new File("output.stax.xml"));

		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		t.transform(new DOMSource(root), result);

	}

}