package com.epam.rd.java.basic.task8.model;

import java.util.LinkedHashMap;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private LinkedHashMap<String, Object> visualParameters = new LinkedHashMap<>();
    private LinkedHashMap<String, Object> growingTips = new LinkedHashMap<>();
    private String multiplying;

    public Flower() {}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }
    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getMultiplying() {
        return multiplying;
    }
    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getOrigin() {
        return origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public LinkedHashMap<String, Object> getVisualParameters() {
        return visualParameters;
    }

    public LinkedHashMap<String, Object> getGrowingTips() {
        return growingTips;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
